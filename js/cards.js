

function changeTheme(val) {
    var body = document.getElementById("body");
    body.setAttribute("style", "background-color:" + val);
}

function submit() {
    var body = document.getElementById("body");
    var name = document.getElementById("name").value;


    var genders = document.getElementsByName("gender");
    var gender = "";
    genders.forEach(gen => {
        if (gen.checked) {
            gender = gen.value;
        }
    });

    var birthdate = document.getElementById("birthdate");
    var email = document.getElementById("email");
    var phone = document.getElementById("phone");

    var themes = document.getElementsByName("theme");
    var theme = "";
    themes.forEach(th => {
        if (th.checked) {
            theme = th.value;
        }
    });

    var country = document.getElementById("country");
    var city = document.getElementById("city");
    var comment = document.getElementById("comment");

    var card = {
        "body": body,
        "name": name,
        "gender": gender,
        "birthdate": birthdate,
        "email": email,
        "phone":phone,
        "country": country,
        "city": city,
        "theme": theme,
        "comment": comment
    };
    
}